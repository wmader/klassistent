/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import de.mad3r.dataBackend 1.0


Kirigami.ApplicationWindow {
    id: appWindow

    title: i18nc("@title:window", "Klassistent")

    pageStack.initialPage: Kirigami.Page {
        Controls.Label {
            anchors.centerIn: parent

            Text {
                id: helloBackend
                color: "white"
                text: DataBE.introductionText
            }
        }
    }
}
