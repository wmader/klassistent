/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include "dataBackend.h"

DataBackend::DataBackend(QObject *parent)
    : QObject(parent)
{
}

auto DataBackend::introductionText() const -> QString
{
    return m_introductionText;
}

auto DataBackend::setIntroductionText(const QString &introductionText) -> void
{
    if (m_introductionText != introductionText) {
        m_introductionText = introductionText;
        Q_EMIT introductionTextChanged();
    }
}
