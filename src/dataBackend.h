/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#pragma once

#include <QObject>

class DataBackend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString introductionText READ introductionText WRITE setIntroductionText NOTIFY introductionTextChanged)

public:
    explicit DataBackend(QObject *parent = nullptr);

    auto introductionText() const -> QString;
    auto setIntroductionText(const QString &introductionText) -> void;
    Q_SIGNAL void introductionTextChanged();

private:
    QString m_introductionText = QStringLiteral("Hello Backend!");
};
