/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include <utility>

#include "student.h"

Name::Name(const QString &first, const QString &last, const QString &middle)
    : m_first{first}
    , m_last{last}
    , m_middle{middle}
{
}

auto Name::first() const -> const QString &
{
    return m_first;
}

auto Name::last() const -> const QString &
{
    return m_last;
}

auto Name::middle() const -> const QString &
{
    return m_middle;
}

auto Name::first(const QString &firstName) -> void
{
    m_first = firstName;
}

auto Name::last(const QString &lastName) -> void
{
    m_last = lastName;
}

auto Name::middle(const QString &middleName) -> void
{
    m_middle = middleName;
}

ClassCode::ClassCode()
    : m_grade(0)
    , m_code(QStringLiteral(""))
    , m_year(0)
{
}

ClassCode::ClassCode(const unsigned int &grade, const QString &code, const unsigned int &year)
    : m_grade(grade)
    , m_code(code)
    , m_year(year)
{
}

auto ClassCode::grade() const -> std::optional<unsigned int>
{
    return m_grade == 0 ? std::nullopt : std::optional<unsigned int>(m_grade);
}

auto ClassCode::code() const -> const QString &
{
    return m_code;
}

auto ClassCode::classKey() const -> std::optional<QString>
{
    return (m_grade == 0 || m_year == 0) ? std::nullopt : std::optional<QString>(QString::number(m_grade) + m_code + QString::number(m_year));
}

void ClassCode::classCode(const unsigned int &grade, const QString &code, const unsigned int &year)
{
    m_grade = grade;
    m_code = code;
    m_year = year;
}

PhoneNumberStore::PhoneNumberStore(const std::unordered_map<PhoneNumberKey, unsigned int> &phoneNumbers)
{
    m_phoneNumbers = phoneNumbers;
}

auto PhoneNumberStore::number(const PhoneNumberKey &key) const -> std::optional<unsigned int>
{
    return m_phoneNumbers.contains(key) ? std::optional<unsigned int>(m_phoneNumbers.at(key)) : std::nullopt;
}

auto PhoneNumberStore::number(const PhoneNumberKey &key, const unsigned int &number) -> void
{
    m_phoneNumbers.insert_or_assign(key, number);
}

EMailAddressStore::EMailAddressStore(const std::unordered_map<EMailAddressKey, QString> &eMailAddresses)
{
    m_eMailAddresses = eMailAddresses;
}

auto EMailAddressStore::eMailAddress(const EMailAddressKey &key) const -> std::optional<QString>
{
    return m_eMailAddresses.contains(key) ? std::optional<QString>(m_eMailAddresses.at(key)) : std::nullopt;
}

auto EMailAddressStore::eMailAddress(const EMailAddressKey &key, const QString &eMailAddress) -> void
{
    m_eMailAddresses.insert_or_assign(key, eMailAddress);
}

Student::Student(const QString &firstName,
                 const QString &lastName,
                 const unsigned int &grade,
                 const QString &code,
                 const unsigned int &year,
                 const QString &middleName,
                 const std::unordered_map<PhoneNumberKey, unsigned int> &phoneNumbers,
                 const std::unordered_map<EMailAddressKey, QString> &eMailAddresses)
    : m_name{firstName, lastName, middleName}
    , m_classCode{grade, code, year}
    , m_phoneNumbers{phoneNumbers}
    , m_eMailAddresses{eMailAddresses}
{
}

auto Student::name() -> Name &
{
    return m_name;
}

auto Student::classInformation() -> ClassCode &
{
    return m_classCode;
}

auto Student::phoneNumber() -> PhoneNumberStore &
{
    return m_phoneNumbers;
}

auto Student::eMailAddress() -> EMailAddressStore &
{
    return m_eMailAddresses;
}
