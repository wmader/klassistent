/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#pragma once

#include <optional>
#include <unordered_map>

#include <QString>
#include <QtCore>

/**
 * @brief Modeling a name supporting first, last, and middle name.
 *
 */
class Name
{
public:
    /**
     * @brief Default constructor
     *
     */
    Name() = default;
    /**
     * @brief Create a name object with first and last name, possibly with middle name.
     *
     * @param first The given name.
     * @param last The family name.
     * @param middle The middle name, defaults to an empty QString.
     */
    Name(const QString &first, const QString &last, const QString &middle = QStringLiteral(""));

    /**
     * @brief Get first name.
     *
     * @return The given name.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto first() const -> const QString &;
    /**
     * @brief Get last name.
     *
     * @return The family name.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto last() const -> const QString &;
    /**
     * @brief Get middle name.
     *
     * @return The middle name.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto middle() const -> const QString &;

    /**
     * @brief Set the first name.
     *
     * @param firstName Given name.
     */
    auto first(const QString &firstName) -> void;
    /**
     * @brief Set the last name.
     *
     * @param lastName Family name.
     */
    auto last(const QString &lastName) -> void;
    /**
     * @brief Set the middle name.
     *
     * @param middleName Middle name.
     */
    auto middle(const QString &middleName) -> void;

private:
    QString m_first;
    QString m_last;
    QString m_middle;
};

/**
 * @brief Models a unique class level of a student, e.g. 10c in 2019.
 *
 * A unique class level describes
 * - the term or class level, e.g., 10-th grade
 * - the cohort in that class level, e.g., out of 4 10-th grad classes, the third, usually denoted by the letter 'c'.
 * - the year for which that information is valid, e.g., 2019.
 *
 * In the example 10c the different entities are
 * Entity     | Content
 * ---------- | -------
 * grade      | 10
 * code       | c
 * year       | 2019
 * class code | 10c
 * class key  | 10c2019
 *
 * The class key is used as a unique identifier a particular class.
 */
class ClassCode
{
public:
    /**
     * @brief Default constructor
     *
     */
    ClassCode();
    /**
     * @brief Constructor
     *
     * @param grade The grade, e.g., 10.
     * @param code The class code, e.g., 'c'
     * @param year The year for which that information is valid, e.g., 2019.
     */
    ClassCode(const unsigned int &grade, const QString &code, const unsigned int &year);

    /**
     * @brief Get the grade
     *
     * @return int
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto grade() const -> std::optional<unsigned int>;
    /**
     * @brief Get the class code
     *
     * @return The class code
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto code() const -> const QString &;
    /**
     * @brief Get the class key.
     *
     * @return The class key.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto classKey() const -> std::optional<QString>;

    /**
     * @brief Set the entire class code.
     *
     * @param grade The grade, e.g., 10.
     * @param code The class code, e.g., 'c'
     * @param year The year for which that information is valid, e.g., 2019.
     */
    auto classCode(const unsigned int &grade, const QString &code, const unsigned int &year) -> void;

private:
    unsigned int m_grade;
    QString m_code;
    unsigned int m_year;
};

enum PhoneNumberKey {
    mobileGeneral,
    mobileMother,
    mobileFather,
    mobileStudent,
    landlineGeneral,
    landlineMother,
    landlineFather,
    landlineStudent,
};

/**
 * @brief Class to store several phone numbers.
 *
 * This class stores phone numbers, one for each key defined by enum PhoneNumberKey.
 */
class PhoneNumberStore
{
public:
    /**
     * @brief Default constructor.
     *
     */
    PhoneNumberStore() = default;
    /**
     * @brief Construct a phone number store from a set of phone numbers.
     *
     * @param Initial Set of phone numbers to be stored.
     */
    PhoneNumberStore(const std::unordered_map<PhoneNumberKey, unsigned int> &phoneNumbers);

    /**
     * @brief Get phone number as requested by key.
     *
     * @param key Key of phone number, see enum PhoneNumberType.
     * @return Respective phone number.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto number(const PhoneNumberKey &key) const -> std::optional<unsigned int>;

    /**
     * @brief Add a phone number
     *
     * @param key Key of phone number, see enum PhoneNumberType.
     * @param number Phone number.
     */
    auto number(const PhoneNumberKey &key, const unsigned int &number) -> void;

private:
    std::unordered_map<PhoneNumberKey, unsigned int> m_phoneNumbers;
};

enum EMailAddressKey {
    general,
    mother,
    father,
    student,
};

/**
 * @brief Class to store several email addresses.
 *
 * This class stores email addresses, one for each key defined by enum EMailAddressKey.
 */
class EMailAddressStore
{
public:
    /**
     * @brief Default constructor.
     *
     */
    EMailAddressStore() = default;

    /**
     * @brief Construct an email address store from a set of email addresses.
     *
     * @param eMailAddresses Initial set of email addresses to be stored.
     */
    EMailAddressStore(const std::unordered_map<EMailAddressKey, QString> &eMailAddresses);

    /**
     * @brief Get email address as requested by key.
     *
     * @param key Key of phone number, see enum PhoneNumberType.
     * @return Respective email address.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto eMailAddress(const EMailAddressKey &key) const -> std::optional<QString>;

    /**
     * @brief Add an email address.
     *
     * @param key Key of email address, see enum EMailAddressKey.
     * @param eMailAddress Email address.
     */
    auto eMailAddress(const EMailAddressKey &key, const QString &eMailAddress) -> void;

private:
    std::unordered_map<EMailAddressKey, QString> m_eMailAddresses;
};

/**
 * @brief Models a student.
 *
 * A student is someone attending a school.
 *
 */
class Student
{
public:
    /**
     * @brief Default constructor.
     *
     */
    Student() = default;
    /**
     * @brief Contruct a student from basic information.
     *
     * @param firstName Student's give name.
     * @param lastName Student's family name.
     * @param grade Student's grade, e.g., 10.
     * @param code Student's class code, e.g., c
     * @param year The year for which that information is valid, e.g., 2019
     * @param middleName Student's middel name, defaults to an empty QString.
     * @param  Set of phone numbers, defaults to empty.
     * @param  Set of phone email addresses, defaults to empty.
     */
    Student(const QString &firstName,
            const QString &lastName,
            const unsigned int &grade,
            const QString &code,
            const unsigned int &year,
            const QString &middleName = QStringLiteral(""),
            const std::unordered_map<PhoneNumberKey, unsigned int> &phoneNumbers = std::unordered_map<PhoneNumberKey, unsigned int>{},
            const std::unordered_map<EMailAddressKey, QString> &eMailAddresses = std::unordered_map<EMailAddressKey, QString>{});

    /**
     * @brief Access student's name.
     *
     * @return Name object of the student.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto name() -> Name &;
    /**
     * @brief Access student's class information.
     *
     * @return ClassCode object of the student.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto classInformation() -> ClassCode &;
    /**
     * @brief Access phone numbers of the student.
     *
     * @return Phone number store of the student.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto phoneNumber() -> PhoneNumberStore &;
    /**
     * @brief Access email addresses of the student.
     *
     * @return Email address store object of the student.
     */
    [[nodiscard("Omitting return value renders call useless.")]] auto eMailAddress() -> EMailAddressStore &;

private:
    Name m_name;
    ClassCode m_classCode;
    PhoneNumberStore m_phoneNumbers;
    EMailAddressStore m_eMailAddresses;
};
