/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include <gtest/gtest.h>

#include "student.h"

class ClassCodeTest : public testing::Test
{
protected:
    auto SetUp() -> void override
    {
        populated.classCode(10, QStringLiteral("c"), 2019);
    }

    ClassCode populated;
    ClassCode empty;
};

TEST_F(ClassCodeTest, BasicDataContent)
{
    EXPECT_EQ(10, populated.grade().value());
    EXPECT_EQ("c", populated.code().toStdString());
    EXPECT_EQ("10c2019", populated.classKey().value().toStdString());

    EXPECT_FALSE(empty.grade().has_value());
    EXPECT_TRUE(empty.code().isEmpty());
    EXPECT_FALSE(empty.classKey().has_value());
}
