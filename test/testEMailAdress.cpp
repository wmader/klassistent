/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include <gtest/gtest.h>

#include "student.h"

TEST(EMailAddressTest, InsertRetrieve)
{
    const QString eMailAddressA = QStringLiteral("family@gmx.de");
    const QString eMailAddressB = QStringLiteral("agnes@web.de");
    const QString eMailAddressC = QStringLiteral("edis_grupp@google.com");

    EMailAddressStore addressStore{};
    addressStore.eMailAddress(EMailAddressKey::general, eMailAddressA);
    addressStore.eMailAddress(EMailAddressKey::mother, eMailAddressB);

    EXPECT_EQ(eMailAddressA, addressStore.eMailAddress(EMailAddressKey::general).value());
    EXPECT_EQ(eMailAddressB, addressStore.eMailAddress(EMailAddressKey::mother).value());
    EXPECT_EQ(std::nullopt, addressStore.eMailAddress(EMailAddressKey::student));

    addressStore.eMailAddress(EMailAddressKey::general, eMailAddressC);
    EXPECT_EQ(eMailAddressC, addressStore.eMailAddress(EMailAddressKey::general).value());
}
