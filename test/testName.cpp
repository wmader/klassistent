/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include <gtest/gtest.h>

#include "student.h"

class NameTest : public testing::Test
{
protected:
    auto SetUp() -> void override
    {
        fooBarBaz.first(QStringLiteral("Foo"));
        fooBarBaz.last(QStringLiteral("Bar"));
        fooBarBaz.middle(QStringLiteral("Baz"));
    }

    Name fooBarBaz;
    Name empty;
};

TEST_F(NameTest, BasicDataContent)
{
    EXPECT_EQ("Foo", fooBarBaz.first().toStdString());
    EXPECT_EQ("Bar", fooBarBaz.last().toStdString());
    EXPECT_EQ("Baz", fooBarBaz.middle().toStdString());

    EXPECT_TRUE(empty.first().isEmpty());
    EXPECT_TRUE(empty.last().isEmpty());
    EXPECT_TRUE(empty.middle().isEmpty());
}
