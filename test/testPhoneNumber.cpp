/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include <gtest/gtest.h>

#include "student.h"

TEST(PhoneNumberTest, InsertRetrieve)
{
    constexpr unsigned int phoneNumberA = 123456789;
    constexpr unsigned int phoneNumberB = 987654321;
    constexpr unsigned int phoneNumberC = 55523176;

    PhoneNumberStore numberStore{};
    numberStore.number(PhoneNumberKey::mobileGeneral, phoneNumberA);
    numberStore.number(PhoneNumberKey::landlineMother, phoneNumberB);

    EXPECT_EQ(phoneNumberA, numberStore.number(PhoneNumberKey::mobileGeneral).value());
    EXPECT_EQ(phoneNumberB, numberStore.number(PhoneNumberKey::landlineMother).value());
    EXPECT_EQ(std::nullopt, numberStore.number(PhoneNumberKey::mobileStudent));

    numberStore.number(PhoneNumberKey::mobileGeneral, phoneNumberC);
    EXPECT_EQ(phoneNumberC, numberStore.number(PhoneNumberKey::mobileGeneral).value());
}
