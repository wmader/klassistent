/*
 *   SPDX-FileCopyrightText: 2023 Wolfgang Mader <Wolfgang_Mader@gmx.de>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include <gtest/gtest.h>

#include "student.h"

class StudentTest : public testing::Test
{
protected:
    void SetUp() override
    {
        phoneNumbers.insert(std::make_pair(mobileGeneral, numberGeneral));
        phoneNumbers.insert(std::make_pair(mobileMother, numberMother));
    }

    const QString firstName = QStringLiteral("Hans");
    const QString lastName = QStringLiteral("Wurst");
    const QString middleName = QStringLiteral("Peter");
    const unsigned int grade = 10;
    const QString code = QStringLiteral("c");
    const unsigned int year = 2019;
    const unsigned int numberGeneral = 12345;
    const unsigned int numberMother = 67890;
    const unsigned int numberFather = 13579;
    std::unordered_map<PhoneNumberKey, unsigned int> phoneNumbers{std::make_pair(mobileFather, numberFather)};
};

TEST_F(StudentTest, BasicDataContent)
{
    Student student{};
    student.name().first(firstName);
    student.name().last(lastName);

    EXPECT_EQ(firstName, student.name().first());
    EXPECT_EQ(lastName, student.name().last());
}

TEST_F(StudentTest, CreateStudentFromBasicData)
{
    Student basicStudent{firstName, lastName, grade, code, year};

    EXPECT_EQ(firstName, basicStudent.name().first());
    EXPECT_EQ(lastName, basicStudent.name().last());
    EXPECT_EQ(QStringLiteral(""), basicStudent.name().middle());
}

TEST_F(StudentTest, CreateStudentFromFullData)
{
    Student basicStudent{firstName, lastName, grade, code, year, middleName, phoneNumbers};

    EXPECT_EQ(firstName, basicStudent.name().first());
    EXPECT_EQ(lastName, basicStudent.name().last());
    EXPECT_EQ(middleName, basicStudent.name().middle());
    EXPECT_EQ(numberGeneral, basicStudent.phoneNumber().number(mobileGeneral));
    EXPECT_EQ(numberMother, basicStudent.phoneNumber().number(mobileMother));
    EXPECT_EQ(numberFather, basicStudent.phoneNumber().number(mobileFather));
}
